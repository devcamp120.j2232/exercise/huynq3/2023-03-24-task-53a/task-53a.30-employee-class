import com.devcamp.Employee;
import java.text.DecimalFormat;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

    Employee employee1= new Employee(5, "Nguyen", "Quoc Huy", 10000000);
    Employee employee2= new Employee(6, "Vu", "Thi Yen", 15000000);      

    // float myFloat1 = employee1.raiseSalary(20);
    // DecimalFormat df1 = new DecimalFormat("###,###.##"); // định dạng số
    // String formattedNumber1 = df1.format(myFloat1);

    // float myFloat2 = employee2.raiseSalary(15);
    // DecimalFormat df2 = new DecimalFormat("###,###.##"); // định dạng số
    // String formattedNumber2 = df2.format(myFloat2);
   

    System.out.println(employee1.toString());
    System.out.println("Ho ten employee1:"+ employee1.toStringFullName());
   
    System.out.println("1 Year- Salary Employee1: "+employee1.getAnnualSalary());
    System.out.println("tăng lương 20%, lương tháng sau: "+ employee1.raiseSalary(20));
    // System.out.println("Formatted Lương: " + formattedNumber1);

    System.out.println(employee2.toString());    
    System.out.println("Ho ten employee2:"+employee2.toStringFullName());
    System.out.println("1 Year -Salary Employee2: "+employee2.getAnnualSalary());
    System.out.println("tăng lương 15%, lương tháng sau: "+ employee2.raiseSalary(15));
   // System.out.println("Formatted Lương: " + formattedNumber2);

        

    }

    
      
}
