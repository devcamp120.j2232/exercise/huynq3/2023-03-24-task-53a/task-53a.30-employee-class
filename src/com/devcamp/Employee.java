package com.devcamp;
import java.text.DecimalFormat;


public class Employee {
    private int id;
    private String firstName;
    private String lastName;
    private float salary;
    public Employee(int id, String firstName, String lastName, float salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastfloaName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public float getSalary() {
        return salary;
    }
    public void setSalary(float salary) {
        this.salary = salary;
    }
    // ham tinh luong theo nam
    public float getAnnualSalary(){
        return salary * 12;
    }
    //ham tinh luong he so
    public String raiseSalary(float percent){
       
        System.out.println("In thử: "+ salary * (1 + percent /100));
        float newSalary=salary * (1 + percent /100);
        
        DecimalFormat df = new DecimalFormat("###,###.##"); // định dạng số
        String formattedNumber = df.format(newSalary);
        
        return formattedNumber;
      
    }
    //ham toString
    public String toString(){
        return "Emlpoyee [id= "+id +", "+firstName+" "+lastName+", salary= "+ salary+"]";
    }
    public String toStringFullName(){
        return "FullName : "+ firstName+" "+lastName;
    }
   
}

